console.log("iniciando server.....");

//utilizamos la libreria express
var express=require('express');

//para poder leer el contenido del body
var bodyparser=require("body-parser");

//para utilizar la libreria de busqueda en un json
var jsonQuery=require("json-query");

//para utilizar la libreria de invocacion rest
var requestJson=require("request-json");

//de claramos objeto api del tipo express instancia
var app=express();

var fs = require('fs');

//metemos que la instancia de express utilice el bodyparser para obtener el body de la peticion post
app.use(bodyparser.json());



/********************version 01 de la api con mlab*******************/
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techumxmv/collections";
var apiKey="apiKey=8uR_dn6cuIxzuUioa6kZmT4Ykxooiq7l";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);

/****************API Movimientos****************/
//get movimientos de una cuenta v01
app.get("/v01/cuentamovimientos/:id",function(req,res)
{
	//https://api.mlab.com/api/1/databases/techumx/collections/cuentas?q={"idcuenta":"CTA000001"}&f={"movimientos":1}&apiKey=50c5ea68e4b0a97d668bc84a
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas");
  clienteMlab.get('?q={"idcuenta":"'+req.params.id+'"}&f={"movimientos":1}&'+apiKey,function(err,resM,body)
  {
		console.log("resM------->"+JSON.stringify(resM));
		if(resM.statusCode!=200)
		{
			res.send("{'movimientosCuenta':'error'}");
		}else
		{
			res.send(body);
		}
  });
});


//post alta movimiento v01
app.post("/v01/cuentamovimientos",function(req,res)
{
	//obtenemos usuarios mlab
	clienteMlab=requestJson.createClient(urlMlabRaiz+"/movimientos?"+apiKey);
	//metemos en un json el email a insertar en collection sesion
	console.log(urlMlabRaiz+"/movimientos?"+apiKey);
	clienteMlab.post("",req.body,function(err,resM,body)
	{
		console.log("resM------->"+JSON.stringify(resM));
		if(resM.statusCode!=200)
		{
			res.send("{'insercionMovimiento':'error'}");
		}else
		{
			res.send("{'insercionMovimiento':'ok'}");
		}
	});
});

/****************API Cuentas****************/
//get cuentas de ese usuario  v01
app.get("/v01/usuariocuentas/:id",function(req,res)
{
	//https://api.mlab.com/api/1/databases/techumx/collections/cuentas?q={"idusuario":"USR000001"}&f={"idcuenta":1}&apiKey=50c5ea68e4b0a97d668bc84a
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas");
  clienteMlab.get('?q={"idusuario":"'+req.params.id+'"}&f={"idcuenta":1}&'+apiKey,function(err,resM,body)
  {
    res.send(body);
  });
});

/****************API Usuarios****************/

//get usuarios v01
app.get("/v01/usuarios",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
  clienteMlab.get("",req.body,function(err,resM,body)
  {
		console.log("_id----->"+body[0]._id.$oid);
		if(resM.statusCode!=200)
		{
			res.send("{'consultaUsuarios':'error'}");
		}else
		{
			res.send(body);
		}
  });
});

//post alta usuario v01
app.post("/v01/usuarios",function(req,res)
{
	//obtenemos usuarios mlab
	clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
	//metemos en un json el email a insertar en collection sesion
	clienteMlab.post("",req.body,function(err,resM,body)
	{
		console.log("inserto el usuario");
		if(resM.statusCode!=200)
		{
			res.send("{'alta':'error'}")
		}else
		{
			res.send("{'alta':'ok'}")
		}
	});
});

//post login v01
app.post("/v01/usuarios/login",function(req,res)
{
	var password=req.headers["password"];
	var email=req.headers["email"];
	console.log("password="+password+", email="+email);
	var resultado;
	var daerror=false;
	//obtenemos usuarios mlab
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
  clienteMlab.get("", function(err,resM,body)
  {
		console.log("=========>error: "+err);
    if(!err)
    {
			usuariosJSON=body;
			console.log("=========>en if: !err");
			//console.log(usuariosJSON);
			//console.log("body respuesta=====>"+JSON.stringify(body));
			resultado=jsonQuery("[email="+email+"]",{data:usuariosJSON});

			if(resultado.value!=null)
			{
				console.log("2esultado.value.password======>");
				console.log(resultado.value.password);
				if(resultado.value.password==password)
				{
					//obtenemos los usuarios en collection sesion
					console.log("resultado======>");
					//console.log(resultado);
					clienteMlab=requestJson.createClient(urlMlabRaiz+"/sesion?"+apiKey);
					clienteMlab.get("", function(err,resM,body)
					{
						console.log("3en peticion collection sesion");
						if(!err)
						{
							//traemos la respuesta de todos los documentos en la collection sesion
							var usuariosSesJSON=body;
							//console.log("4body respuesta en sesion=====>"+JSON.stringify(body));
							//hacemos la busqueda del email con el que estan haciendo login
							var resultadoSes=jsonQuery("[email="+email+"]",{data:usuariosSesJSON});
							//entra si no encuentra el correo en la collection sesion
							if(resultadoSes.value!=null)
							{
								console.log("5entro en resultado trae datos");
							}else
							{
								console.log("6dentro de no hay email del usuario en collection sesion");
								//insertamos el email en la colection "sesion" para saber que este usuario esta adentro
								clienteMlab=requestJson.createClient(urlMlabRaiz+"/sesion?"+apiKey);
								//metemos en un json el email a insertar en collection sesion
								var data= {"email":email};
								clienteMlab.post("",data,function(err,resM,body)
								{
									if(resM.statusCode!=200)
									{
										daerror=true;
										console.log("estatus no 200");
									}else
									{
										daerror=false;
										console.log("estatus si 200");
									}
									return console.log("inserto en la tabla sesion");
								});
							}
						}
					});
					if(daerror)
					{
						res.send('');
						console.log('8{"login":"error"}');
					}else
					{
						//res.send(""+resultado.value.idusuario);
						res.send('{"login":"ok","idusuario":"'+resultado.value.idusuario+'","oid":"'+resultado.value._id.$oid+'","email":"'+resultado.value.email+'"}');
						console.log('{"login":"ok","idusuario":"'+resultado.value.idusuario+'","oid":"'+resultado.value._id.$oid+'","email":"'+resultado.value.email+'"}');
					}
				}else
				{
					res.send('');
					console.log('10{"login":"error"}');
				}
			}else
			{
				res.send('');
				console.log('11{"login":"error"}');
			}
		}else
		{
			res.send('');
			console.log('12{"login":"error"}');
		}
	});
});

//put modifica usuario v01
app.put("/v01/usuarios/:id",function(req,res)
{
	clienteMlab = requestJson.createClient("https://api.mlab.com/api/1/databases/techumxmv/collections/usuarios");
  var cambio = '{"$set":'+ JSON.stringify(req.body) + '}';
  clienteMlab.put('?q={"idusuario":"'+req.params.id+'"}&'+apiKey,JSON.parse(cambio),function(err,resM,body)
  {
		console.log("update resM------->"+JSON.stringify(resM.statusCode));
		if(resM.statusCode!=200)
		{
			res.send("{'actualizado':'error'}");
		}else
		{
			res.send("{'actualizado':'ok'}");
		}
  });
});

//delete usuario v01
app.delete("/v01/usuarios/:id",function(req,res)
{
	clienteMlab = requestJson.createClient("https://api.mlab.com/api/1/databases/techumxmv/collections/usuarios/"+req.params.id);
	//console.log("dentro ----> https://api.mlab.com/api/1/databases/techumxmv/collections/usuarios/"+req.params.id+'?'+apiKey);
  clienteMlab.del('?'+apiKey,function(err,resM,body)
  {
		console.log("resM------->"+JSON.stringify(resM.statusCode));
		if(resM.statusCode!=200)
		{
			console.log('{"borrado":"error"}');
			res.send('{"borrado":"error"}');
		}else
		{
			console.log('{"borrado":"ok"}');
			res.send('{"borrado":"ok"}');
		}
  });
});


//delete logout v01 manda a borrar la sesion mandando el correo
app.delete("/v01/logout/:id",function(req,res)
{
	clienteMlab=requestJson.createClient(urlMlabRaiz+"/sesion?"+apiKey);
  clienteMlab.get("",req.body,function(err,resM,body)
  {
		console.log("======> en logout estatus"+resM.statusCode);
		if(resM.statusCode!=200)
		{
			res.send('{"logout":"error"}');
		}else
		{
			usuariosJSON=body;
			//buscamos el correo
			resultado=jsonQuery("[email="+req.params.id+"]",{data:usuariosJSON});
			//lo guardamos si lo encuentra
			if(resultado.value!=null)
			{
				console.log(resultado.value);
				resultado=""+resultado.value._id.$oid;
				clienteMlab = requestJson.createClient("https://api.mlab.com/api/1/databases/techumxmv/collections/sesion/"+resultado);
				//console.log("dentro ----> https://api.mlab.com/api/1/databases/techumxmv/collections/usuarios/"+req.params.id+'?'+apiKey);
			  clienteMlab.del('?'+apiKey,function(err,resM,body)
			  {
					console.log("resM------->"+JSON.stringify(resM.statusCode));
					if(resM.statusCode!=200)
					{
						console.log('{"logout":"error"}');
						res.send('{"logout":"error"}');
					}else
					{
						console.log('{"logout":"ok"}');
						res.send('{"logout":"ok"}');
					}
			  });
			}else
			{
				res.send('{"logout":"error"}');
			}
		}
  });
});

/////////////////////////sesion/////////////////////////////
//get sesion v1 revisa que el usuario este logeado con base al correo
app.get("/v01/sesion/:id",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/sesion?"+apiKey);
  clienteMlab.get("",req.body,function(err,resM,body)
  {
		console.log("sesion estatus code==>"+resM.statusCode);
		if(resM.statusCode!=200)
		{
			res.send('{"consultaUsuarios":"error"}');
		}else
		{
			usuariosJSON=body;
			resultado=jsonQuery("[email="+req.params.id+"]",{data:usuariosJSON});
			if(resultado.value!=null)
			{
				console.log(resultado.value);
				res.send('{"consultaUsuarios":"ok"}');
			}else
			{
				res.send('{"consultaUsuarios":"error"}');
			}
		}
  });
});

//sesion sin parametros
app.get("/v01/sesion/",function(req,res)
{
	res.send('{"consultaUsuarios":"error"}');
});

app.listen(3000);
console.log("escuchando en el puerto 3000...");
