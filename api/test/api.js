var mocha=require("mocha");
var chai=require("chai");
var chaiHttp=require("chai-http");
var should=chai.should();
var server=require("../server");//se puede poner server sin el server.js
chai.use(chaiHttp);//configurar modulo  chai https

describe("Test de conectividad",() => {
  it("Google funciona", (done) => {
    chai.request("http://www.google.com.mx")
    .get("/")
    .end((err,res) => {
        //console.log(res)
        res.should.have.status(200);
        done();
      })
  })
});

describe("Test de api usuarios",() => {
  it("Api raiz funciona", (done) => {
    chai.request("http://localhost:3000")
    .get("/v3")
    .end((err,res) => {
        //console.log(res)
        res.should.have.status(200);
        done();
      })
  })
});

describe("Test de api usuarios funciona",() => {
  it("Api raiz funciona", (done) => {
    chai.request("http://localhost:3000")
    .get("/v3")
    .end((err,res) => {
        //console.log(res)
        res.should.have.status(200);
        res.body.should.be.a("array");
        done();
      })
  })
});

describe("Test de api usuarios funciona devuelve 2 colecciones",() => {
  it("Api raiz funciona", (done) => {
    chai.request("http://localhost:3000")
    .get("/v3")
    .end((err,res) => {
        //console.log(res)
        res.should.have.status(200);
        res.body.should.be.a("array");
        res.body.length.should.be.eql(2);
        done();
      })
  })
});

describe("Test de api usuarios funciona devuelve los objetos corrector",() => {
  it("Api raiz funciona", (done) => {
    chai.request("http://localhost:3000")
    .get("/v3")
    .end((err,res) => {
        //console.log(res)
        res.should.have.status(200);
        res.body.should.be.a("array");
        res.body.length.should.be.eql(2);
        for (var i = 0; i < res.body.length; i++) {
          res.body[i].should.have.property("recurso");
          res.body[i].should.have.property("url");
        }
        done();
      })
  })
});

var tamano=0;

describe("Test de api movimientos v3",() => {
  it("Api raiz api movmientos funciona v3", (done) => {
    chai.request("http://localhost:3000")
    .get("/v3/movimientos")
    .end((err,res) => {
				tamano=res.body.length;
				//console.log("tamaño======>"+tamano);
				//console.log(res);
        res.should.have.status(200);
        res.body.should.be.a("array");
        done();
      })
  })
});



describe("Test de api movimientos v3 con un movimiento",() =>
{
	for (var i = 0; i < 50; i++)
	{
	  it("Api raiz api movimientos funciona v3 con un movimiento id:"+(i+1), (done) =>
	  {
				//console.log("en movimiento "+(i+1)+" url=http://localhost:3000/v3/movimientos/"+(i+1));
				chai.request("http://localhost:3000")
				.get("/v3/movimientos/"+(i+1))
				.end((err,res) =>
				{
						//console.log("res.body[0]="+res.body[0]);
						res.should.have.status(200);
						res.body[0].should.have.property("id");
						res.body[0].id.should.have.eql(i+1);
						res.body.should.be.a("array");
						res.body.length.should.be.eql(1);
						done();
				})
	  })
	}
})
